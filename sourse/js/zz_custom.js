(function($) {
    var $promoSlider;

    $(document).ready(function(){
        $('input, textarea').placeholder();

        $('.image-colorbox').colorbox({

        });

        $('.inline-colorbox').colorbox({
            inline: true
        });

        $('.b-video__navigation-item').click(function(e){
            e.preventDefault();
            if($(this).hasClass('current')){
                return false;
            }
            $('.b-video__navigation-item').removeClass('current');
            $(this).addClass('current');
            $('#b-video__video').attr('src', $(this).attr('href'));
        });

        $('.b-spoiler__item').click(function(e){
            e.preventDefault();

            var $item = $(this),
                $parent = $(this).closest('.b-spoiler'),
                $spoiler = $parent.find('.b-spoiler__content'),
                $text = $('.text', this).html();
            if($item.hasClass('active') || $item.hasClass('b-spoiler__item_noclick')){
                return false;
            }

            if($parent.find('.b-spoiler__item.active').length){
                $parent.find('.b-spoiler__item').removeClass('active');
                $(this).addClass('active');
                $spoiler.stop().slideUp(50, function(){
                    $spoiler.html($text).stop().slideDown(50, function(){
                        $('html, body').animate({
                            scrollTop: $item.offset().top - 65
                        }, 50);
                    });
                });
            } else {
                $(this).addClass('active');
                $spoiler.html($text).stop().slideDown(50, function(){
                    $('html, body').animate({
                        scrollTop: $item.offset().top - 65
                    }, 50);
                });
            }
        });

        //scroll
        function scrollto_c(elem, time) {
            if(time == "undefined") time = 1000;
            $('html, body').animate({
                scrollTop: $(elem).offset().top - 60
            }, time);
        }

        $('.anim-scroll').click(function () {
            scrollto_c($(this).attr('href'));
            return false;
        });

        $('.b-header__logo').on('click', function(){
            $('html, body').animate({
                scrollTop: 0
            }, 100);
        });

        // Контактная форма
        $('.js-ftext[required]').keyup(function(){
            textValidate($(this));
        });

        $("form" ).on( "submit", function(event) {
            event.preventDefault();

            var $form = $(this);
            if($form.hasClass('sending'))
                return;

            var $filds = $form.find('.js-ftext[required]'),
                sendData =  $form.serialize();

            var  message = {};
            var $output = $form.find('.js-form__output');

            message.empty = false;

            $filds.each(function(){
                if(textValidate($(this)))
                    message.empty = true;
            });

            $output.html('');
            var errHtml = '';

            if(message.empty){
                errHtml += '<p>Заполните все поля</p>';
            }

            if(errHtml){
                $output.addClass('error').removeClass('success').append(errHtml).show('fast', function(){
                    $.colorbox.resize();
                });
                return false;
            } else {
                $output.hide('fast', function(){
                    $.colorbox.resize();
                });
            }

            $.ajax({
                type:"post",
                data: sendData,
                url:"send.php",
                beforeSend: function(){
                    $form.addClass('sending');
                    $('.js-form__submit').addClass('sending');
                },
                complete: function(data){
                    $form.removeClass('sending');
                    $('.js-form__submit').removeClass('sending');
                    $output.append(data.responseText).removeClass('error').show('fast', function(){
                        $.colorbox.resize();
                    });
                    if(data.responseText == "Ваше сообщение отправлено. Спасибо."){
                        $form[0].reset();
                    }

                }
            });
        });

        /*
         * Проверка валидности текстовых полей
         *
         * @param {jQuery object} джейквери объект валидируемого поля.
         *
         * return bool
         * */
        function textValidate(el) {
            var type = el.attr('type');
            if(type == 'text' || type == "email" || type == "tel" || type == ""){
                if(el.val() != ""){
                    el.removeClass('error');
                    return false;
                } else {
                    el.addClass('error');
                    return true;
                }
            }
        };
    });

    $(window).load(function() {
        $('.b-islider').flexslider({
            animation: "slide",
            itemWidth: 245,
            itemMargin: 0,
            minItems: 1,
            maxItems: 3,
            controlNav: false
        });
        $promoSlider = $('.b-promo__type-slider').flexslider({
            controlNav: false,
            directionNav: false,
            slideshow:false
        });
        $('.link', '.b-promo__type-selector').click(function(e){
            e.preventDefault();
            $promoSlider.data('flexslider').flexAnimate($(this).attr('href'));
        });

    });
}(jQuery));